# TamaTrainer
## A Hack the Hub project by Team Cayan

```
pip install -U -r requirements.txt
```

### General Overview

This program will spawn X tournaments of Y tamagotchis. Each Tam
has its own neural network, initialised with random "dna" (weights)
which will give us a diverse population.

After they have all went to tamagotchi heaven, the tournaments are reset
and the "fittest" tam AIs get reinserted into the next generation, along 
with a set of fresh, random tams. This ensures we don't over-fit the
model, and also provides some momentum to get over local minima.

Ideally, the next generation would also contain cross-bred tamagotchis, 
with "dna" from two parent tamagotchis. This was a bit beyond our 
time at the hackathon, but it's really cool! 

@Andernoo (twitter)
@Andrew_Cayan (HtH Slack)

### Why?
If you can look after a virtual pet, you can help look after a real one. Maybe even a people.

### How?
- Main will be the starting point for our AI. Spawn, kill, adapt, survive.
```
python main.py 
```
- Spawn a single tama that you can play with. Cute!

```
python tama_game.py 
```
