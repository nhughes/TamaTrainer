import numpy as np
import json, time
import matplotlib.pyplot as plt
from Tournament import Tournament 

'''
General Overview

This file will spawn X tournaments of Y tamagotchis. Each Tam
has its own neural network, initialised with random "dna" (weights)
which will give us a diverse population.

After they have all went to tamagotchi heaven, the tournaments are reset
and the "fittest" tam AIs get reinserted into the next generation, along 
with a set of fresh, random tams. This ensures we don't over-fit the
model, and also provides some momentum to get over local minima.

Ideally, the next generation would also contain cross-bred tamagotchis, 
with "dna" from two parent tamagotchis. This was a bit beyond our 
time at the hackathon, but it's really cool! 

Any questions, you can email me at adowney@cayan.com (or andrew@anooserve.com),
or any of the other Cayaneers of course!
'''

# Parameters (move to config file?)
numTournaments = 10
numPerTournament = 30
display_step = 100
sleeptime = 2
filename = 'braindumps/topTam-' + str(int(round(time.time() * 1000))) + '.json'
tournaments = []
plotData = []
previousHighscore = 0
figSize = (8, 8)

# Some graphics configuration
# The fitness over time graph
plt.figure(1, figSize)
plt.tight_layout()
plt.xlabel('Generation')
plt.ylabel('Age (ticks)')
# The Neural network graph
plt.figure(2, figSize)
plt.axis('off')
plt.ion()

print('Adding {0} tournaments with {1} tamagotchi(s) each'.format(numTournaments, numPerTournament))
for tournamentId in range(1, numTournaments + 1):
	tournament = Tournament(str(tournamentId).zfill(len(str(numTournaments))), numPerTournament)
	tournaments.append(tournament)

for tournament in tournaments:
	# We start them in a separate loop because it can take a few seconds to spin up the threads
	tournament.start()

while True:
	running = True
	while running:
		running = False
		for tournament in tournaments:
			if tournament.running:
				running = True
				tournament.run()
			
		if(running):
			print('Running tournaments found, sleeping for ' + str(sleeptime))
			time.sleep(sleeptime)

	highscore = type('', (), {'fitness': 0})()
	topTams = []
	for tournament in tournaments:
		results = []
		for tam in tournament.getTop(2):
			results.append(tam.fitness)
			topTams.append(tam)
			if tam.fitness > highscore.fitness:
				highscore = tam
		print('Tournament {0} Top Three:'.format(tournament.tournamentId), results)
		print('Tournament {0} Statistics:'.format(tournament.tournamentId), tournament.getStats())

	print('Longest living tamagotchi was {0} with {1}'.format(highscore.tamId, highscore.fitness))
	plotData.append(highscore.fitness)
	if(previousHighscore != highscore.fitness):
		previousHighscore = highscore.fitness
		plt.figure(2) # select and redraw the neural network graph
		topTams[0].network.draw_neural_net(plt.gca(), 0.1, 0.9, 0.1, 0.9, [5, 10, 10, 6])
	plt.figure(1) # select and redraw the fitness / time graph
	plt.plot(plotData)
	plt.pause(0.05) # quick and dirty hack to provide realtime graphics from matplot
	
	# Sort the tams by how old they were when the bucket was kicked
	# The top tam will be directly reinserted to the next batch
	topTams.sort(key=lambda tam: tam.fitness, reverse=True)

	# dump the best neural network to a file for backup and inspection
	with open(filename, 'w') as f:
		f.write(json.dumps(topTams[0].getWeights()))
		f.write("\n")
		f.write(json.dumps(topTams[0].getActions()))

	# Regenerate the tournaments. This will just clear all of the tams
	# and start from scratch, but re-use the existing threads
	for tournament in tournaments:
		tournament.regen(topTams)
		tournament.start()