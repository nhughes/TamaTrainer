import threading, signal, time
from NeuralNetwork import NeuralNetwork
from Tama import Tama
from random import randint

def mapToRange(x, in_min, in_max, out_min, out_max):
	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min

class Tamagotchi (threading.Thread):
	def __init__(self, tamId, weights = None, spliceWeights = None):
		threading.Thread.__init__(self)
		self.tamId = tamId
		self.tam = Tama()
		self.fitness = 0
		self.running = True
		self.threaded = False
		self.actions = []

		self.network = NeuralNetwork(len(self.tam.get_info()), self.tam.number_of_functions + 1, 10, 2)
		if(weights != None):
			self.network.set_weights(weights)

	def getWeights(self):
		return self.network.get_weights()

	def getActions(self):
		return self.actions

	def run(self):
		self.threaded = True
		while not self.tam.dead:
			self.inputs = self.tam.get_info()
			self.inputs[0] = mapToRange(self.inputs[0], 0, 8, 0, 1)
			self.inputs[1] = mapToRange(self.inputs[0], 0, 8, 0, 1)
			# self.inputs[2] = mapToRange(self.inputs[0], 0, 1, 0, 1)
			# self.inputs[3] = mapToRange(self.inputs[0], 0, 1, 0, 1)
			self.inputs[4] = mapToRange(self.inputs[0], 0, 4, 0, 1)
			self.inputs[5] = mapToRange(self.inputs[4], 0, 3, 0, 1)
			# print(self.tamId, self.inputs)
			
			self.outputs = self.network.update(self.inputs)
			# print(self.tamId, self.outputs)

			self.actions.append(self.outputs.index(max(self.outputs)))
			#print(self.tamId + ' calling function ' + str(self.outputs.index(max(self.outputs))))
			self.tam.run(self.outputs.index(max(self.outputs)))
		
		self.fitness = self.tam.tick 
		self.running = False