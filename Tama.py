ACTION_NOOP = 5
ACTION_CLEAN = 0
ACTION_PLAY = 1
ACTION_CURE = 2
ACTION_FEED_MEAL = 3
ACTION_FEED_SNACK = 4


class Tama:
    def __init__(self):
        self.number_of_functions = 5
        self.tick = 0
        self.hunger = 0  # 0:FULL 4:STARVING
        self.happiness = 8  # 0: SAD 4:SUPER HAPPY
        self.depression_counter = 0
        self.age = 0
        self.sickness = 0  # 0 not sick 8 max
        self.sick = False
        self.dead = False
        self.poop = 0
        self.sleeping = False
        self.energy = 4  # 0-4

        self.limits = [
            {"min": 0, "max": 8},  # hunger
            {'min': 0, 'max': 8},  # happiness
            {'min': 0, 'max': 1},  # sick or not
            {'min': 0, 'max': 1},  # sleeping or not
            {'min': 0, 'max': 4},  # energy
            {'min': 0, 'max': 3},  # poop
            {'min': 0, 'max': 8}   # sickness
        ]

    def get_info(self):
        return [self.hunger, self.happiness, int(self.sick), int(self.sleeping), self.energy, self.poop]

    def clean(self):
        self.poop = 0

    def play(self):
        if self.sleeping:
            self.__update_happiness(-1)
        else:
            self.__update_happiness(1)
            self.__update_energy(-1)

    def cure(self):
        self.sick = False
        self.sickness = 0
        if self.sleeping:
            self.sleeping = False
            self.__update_happiness(-2)
        self.__update_happiness(-2)

    def feed_meal(self):
        if self.sick or self.sleeping:
            # print('nope')
            return
        if self.poop > 2:
            self.__update_happiness(-1)
        if self.hunger == 0:
            self.__update_happiness(-1)
        self.__update_hunger(-1)
        self.__update_poop(1)

    def feed_snack(self):
        if self.sick or self.sleeping:
            return
        self.__update_hunger(-1)
        self.__update_happiness(1)
        self.__update_sickness(1)
        self.__update_poop(1)

    def run(self, function_call):
        self.tick += 1
        self.__check_poop()
        self.__check_dead()
        self.__check_sick()
        self.__check_asleep()
        if self.dead:
            return

        if self.tick % 3 == 0:  # every three hours
            self.__update_hunger(1)
            if self.sleeping:
                self.__update_energy(2)
            else:
                self.__update_happiness(-1)
                self.__update_energy(-1)

        if self.tick % 24 == 0:  # yearly
            self.age += 1

        if self.happiness <= 2:
            self.depression_counter += 1

        if function_call == 0:
            self.clean()
        elif function_call == 1:
            self.play()
        elif function_call == 2:
            self.cure()
        elif function_call == 3:
            self.feed_meal()
        elif function_call == 4:
            self.feed_snack()

    def __check_dead(self):
        if self.hunger >= 8:
            # print("hunger")
            self.dead = True
            return
        if self.age >= 182:
            print("old age")
            self.dead = True
            return
        if self.sickness >= 6:
            # print("sick")
            self.dead = True
            return
        if self.happiness == 0 and self.depression_counter == 6:
            self.dead = True
            # print("depression")
            return

    def __check_asleep(self):
        if self.energy <= 0:
            self.sleeping = True
        if self.energy >= 4:  # wakey wakey
            self.sleeping = False

    def __check_poop(self):
        if self.poop >= 2:
            self.__update_sickness(1)
            self.__update_happiness(-1)

    def __check_sick(self):
        if self.sickness >= 4:
            self.sick = True

    def __update_hunger(self, value):
        if self.limits[0]['min'] <= self.hunger + value <= self.limits[0]['max']:
            # print("updating hunger")
            self.hunger += value

    def __update_happiness(self, value):
        if value > 0:
            self.depression_counter = 0
        if self.limits[1]['min'] <= self.happiness + value <= self.limits[1]['max']:
            self.happiness += value

    def __update_sickness(self, value):
        if self.limits[6]['min'] <= self.sickness + value <= self.limits[6]['max']:
            self.sickness += value

    def __update_poop(self, value):
        if self.limits[5]['min'] <= self.poop + value <= self.limits[5]['max']:
            self.poop += value

    def __update_energy(self, value):
        if self.limits[4]['min'] <= self.energy + value <= self.limits[4]['max']:
            self.energy += value


if __name__ == '__main__':
    potato = Tama()
    while not potato.dead:
        print(potato.get_info())
        action = int(input("kill me please "))
        potato.run(action)


# ACTION_NOOP = 5
# ACTION_CLEAN = 0
# ACTION_PLAY = 1
# ACTION_CURE = 2
# ACTION_FEED_MEAL = 3
# ACTION_FEED_SNACK = 4
# [hunger, happiness, int(self.sick), int(self.sleeping), .energy, .poop]

# Andrew, we got the code working and the AI is able to find the max expectancy (some times early, sometimes later) but
# eventually it gets it. What we would love for the presentation tomorrow is : "Can we have what is the input
# of the functions?
# I mean, can, from the AI, get what the AI is doing to keep them alive the 80 years?
# Thamks for everything :)
